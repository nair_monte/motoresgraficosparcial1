using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo3 : MonoBehaviour
{

    
    public GameObject bala;
    void Start()
    {
        //disparar 1 vez
        StartCoroutine(Disparar());
    }

 
    void Update()
    {
        
    }

    IEnumerator Disparar()
    {
        int cdDisparos = 2;
        yield return new WaitForSeconds(cdDisparos);

        GameObject disparo = Instantiate(bala, transform.position+new Vector3(0,0,1), Quaternion.identity);
       disparo.gameObject.GetComponent<Rigidbody>().velocity += new Vector3(0, 0, 40);
        yield return new WaitForSeconds(2);

        Destroy(disparo);

        //volver a disparar
        StartCoroutine(Disparar());
    }
}
