using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento;
    public Camera camaraPrimerPersona;
   
    
    
    public float magnitudSalto;
     public Rigidbody rb;
    public CapsuleCollider collider;
    public int contSaltos = 0;
    public int maxSaltos = 2;
    public bool estaEnElPiso=true;
    public GameObject jugador;
   

    public TMPro.TMP_Text txtGameOver;
    public Image imgGameOver;
    
    public Image imgCeguera;


    public TMPro.TMP_Text txtNoDisparo;
    public bool sePuedeDisparar;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        rb = GetComponent<Rigidbody>();
        collider = GetComponent<CapsuleCollider>();
        jugador = GameObject.Find("Jugador");
        sePuedeDisparar = true;


    }


    void Update()
    {
        float movAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movIzqDer = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movIzqDer *= Time.deltaTime;
        movAdelanteAtras *= Time.deltaTime;


        transform.Translate(movIzqDer, 0, movAdelanteAtras);




        if (Input.GetKeyDown(KeyCode.Space) && (estaEnElPiso || (contSaltos < maxSaltos)))


        {

            rb.velocity = new Vector3(0f, magnitudSalto, 0f * Time.deltaTime);

            estaEnElPiso = false;
            contSaltos++;

        }






        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        
        
       
        
            if (Input.GetMouseButtonDown(0)&&sePuedeDisparar)
            {
                Ray ray = camaraPrimerPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;

                if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 30)
                {
                    Debug.Log($"el rayo toco a {hit.collider.name}");
                    if (hit.collider.name.Substring(0, 7) == "Enemigo")
                    {
                        GameObject objetoTocado = GameObject.Find(hit.transform.name);
                        ControlEnemigo scriptObjetoTocado = (ControlEnemigo)objetoTocado.GetComponent(typeof(ControlEnemigo));
                        if (scriptObjetoTocado != null)
                        {
                            scriptObjetoTocado.recibirDaņo();

                        }

                    }

                    if (hit.collider.name.Substring(0, 5) == "buffo")

                    {
                        GameObject objetoTocado = GameObject.Find(hit.transform.name);

                        Destroy(objetoTocado);
                        transform.position += new Vector3(0f, 3f, 0f);
                        transform.localScale += new Vector3(1f, 1f, 1f);
                        rapidezDesplazamiento += 1.5f;

                    }
                    if (hit.collider.name == "Armadura")
                    {
                        StartCoroutine(desactivarDisparo());
                    }

                }
            }

        
    }

    private void OnCollisionEnter(Collision collision)
    {
        
                
       
        if (collision.gameObject.tag == "Piso")
        {
            estaEnElPiso = true;
           contSaltos = 0;
        }

                 
        if (collision.gameObject.tag == "Enemigos"|| collision.gameObject.tag =="bala")
        {
          imgGameOver.enabled = true;
            txtGameOver.enabled = true;
            Time.timeScale = 0;
        }

        if (collision.gameObject.tag == "Enemigo4")
        {
            StartCoroutine(Ceguera());
            
        }

              

    }

    IEnumerator Ceguera()
    {
        imgCeguera.enabled = true;
        contSaltos = 2;
        rapidezDesplazamiento = 2f;


        yield return new WaitForSeconds(5);
        imgCeguera.enabled = false;
        contSaltos = 0;
        rapidezDesplazamiento = 5f;
    }


    IEnumerator desactivarDisparo()
    {
        sePuedeDisparar = false;
        txtNoDisparo.enabled = true;
        yield return new WaitForSeconds(3);
        sePuedeDisparar = true;
        txtNoDisparo.enabled = false;
    }


}


