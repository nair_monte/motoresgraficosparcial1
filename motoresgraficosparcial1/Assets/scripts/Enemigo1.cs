using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1 : MonoBehaviour
{
    
    public int rapidez;
    private GameObject jugador;
    private void Start()
    {
        jugador = GameObject.Find("Jugador");
    }
    void Update()
    {
        
        transform.LookAt(jugador.transform);
       
        transform.Translate(rapidez * new Vector3(0,0,1) * Time.deltaTime);
      
    }
}
