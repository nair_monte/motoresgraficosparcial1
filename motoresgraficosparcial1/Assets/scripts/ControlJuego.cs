using System.Collections;

using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ControlJuego : MonoBehaviour

{
    public static ControlJuego instancia;
    public GameObject jugador;

    public float tiempoRestante = 60;
    public TMPro.TMP_Text txtCantidadEnemigos;
    public int TotalAMatar;
    public GameObject contenedorEnemigos;
    public TMPro.TMP_Text txtTiempo;

    public TMPro.TMP_Text txtGameOver;
    public Image imgGameOver;
    public TMPro.TMP_Text txtGanaste;

    public Image imgCeguera;

    public TMPro.TMP_Text txtNoDisparo;

    public GameObject pisoInvisible;

    private int enemigosAsesinados = 0;
    void Start()
    {
        Time.timeScale = 1;
        imgGameOver.enabled = false;
        imgCeguera.enabled = false;
        txtGameOver.enabled = false;
        txtGanaste.enabled = false;
        txtNoDisparo.enabled = false;
        ComenzarJuego();


        instancia = this;

        TotalAMatar = contenedorEnemigos.GetComponentsInChildren<ControlEnemigo>().Length;
        txtCantidadEnemigos.text = $"Cantidad de enemigos: {TotalAMatar.ToString("f0")}";
        txtTiempo.text = "Tiempo: " + tiempoRestante.ToString("f0") + " s";


    }

    void Update()

    {
        cronometro();
        txtTiempo.text = "Tiempo: " + tiempoRestante.ToString("f0") + " s";


        if (tiempoRestante <= 0)
        {
            imgGameOver.enabled = true;
            txtGameOver.enabled = true;
            Time.timeScale = 0;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            reStart();
        }

        ganarJuego();

    }
    void ComenzarJuego()

    {
        jugador.transform.position = new Vector3(2f, 2f, 9f);


        tiempoRestante = 60;
        cronometro();
    }

    public void cronometro()
    {

        tiempoRestante -= Time.deltaTime;
    }

    public void EnemigoMuerto()
    {
        TotalAMatar--;
        enemigosAsesinados++;
        txtCantidadEnemigos.text = $"Cantidad de enemigos: {TotalAMatar.ToString("f0")}";
    }

    public void reStart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Shooter");
    }

    public void ganarJuego()
    {
        if (enemigosAsesinados == 10)
        {
            txtGanaste.enabled = true;
            Time.timeScale = 0;
        }
    }
}